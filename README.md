This is my own version of PaddleBall project developed for my UI class.
I have created separate view elements for paddle, ball and the bricks which interact using the callbacks.
I am open to suggestions as to what can be done better.
Just for information, this project has already been submitted, so I am not asking for help in the assignment. :)
Just curious as to what can be done better.

To set up, you just need to import this source as Android Studio project.

In case of any suggestions or queries, I can be reached at vikram.gaur@utdallas.edu