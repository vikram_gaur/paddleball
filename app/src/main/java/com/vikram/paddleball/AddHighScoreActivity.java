package com.vikram.paddleball;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.vikram.paddleball.utils.Person;

import java.util.ArrayList;

/**
 * High score addition activity.
 * Created by hp-pc on 11/1/2015.
 *
 * @author Vikram Gaur
 */
public class AddHighScoreActivity extends BaseActivity {
    final static String ITEM_ARGUMENT = "score";
    double mScore = -1;
    EditText scoreName;
    TextView scoreView;
    //TODO: update the title as soon as first name is edited.

    /**
     * Entry point for this activity.
     * @param savedInstanceState earlier saved instance of the application
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        provideBackNavigation(true);

        scoreName = (EditText) findViewById(R.id.score_name);
        scoreName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    updateAndStore();
                }
                return false;
            }
        });
        scoreView = (TextView) findViewById(R.id.score);

        mScore = getIntent().getLongExtra(ITEM_ARGUMENT, 0)/1000.0;
        scoreView.setText(String.valueOf(mScore));
        setTitle(R.string.add_high_score);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    // layout to be set.
    @Override
    public int getLayoutResource() {
        return R.layout.add_high_score_view;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_high_score, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_save:
                updateAndStore();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * To store the high score in the system.
     */
    private void updateAndStore() {
        //TODO: if the current record is being modified, then store on top of it else add new high score.
        String name = scoreName.getText().toString();

        Person p = new Person();
        p.setName(applyCorrection(name));
        p.setHighScore(String.format("%.2f", mScore));
        ArrayList<Person> array = operations.getDataRecords();
        int i=0;
        for(; i<array.size(); i++) {
            Person other = array.get(i);
            if(Double.valueOf(other.getHighScore()) > mScore) {
                break;
            }
        }
        try {
            array.add(i, p);
        } catch(IndexOutOfBoundsException ex) {
            array.add(p);
        }
        if(array.size() > 10) {
            // remove excess elements
            for(i=array.size()-1; i>=10; i--) {
                array.remove(i);
            }
        }
        operations.setDataRecords(array);
        operations.saveData();
        launchHighScoresListActivity();
        finish();
    }

    /**
     * Corrects the string passed to it and removes unnecessary spaces.
     * @param message String to be corrected.
     * @return corrected string.
     */
    private String applyCorrection(String message) {
        String[] correction = message.split(" ");
        message = "";
        for (String x : correction) {
            if (!x.isEmpty())
                message += x + " ";
        }
        return message.trim();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
