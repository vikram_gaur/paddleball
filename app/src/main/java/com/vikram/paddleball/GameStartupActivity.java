package com.vikram.paddleball;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.vikram.paddleball.utils.Person;

import java.util.ArrayList;

/**
 * Activity to show the first screen.
 * This will be the activity to launch high scores or game.
 * Created by hp-pc on 11/1/2015.
 *
 * @author Vikram Gaur
 */
public class GameStartupActivity extends BaseActivity {
    private static final String TAG = "GameActivity";
    long score = 0;
    private int MAX_SIZE = 10;

    /**
     * Entry point for the Game startup activity.
     * @param savedInstanceState earlier saved instance of the application
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        Button gameActivityLaunch = (Button) findViewById(R.id.game_launch);
        Button highScoreActivityLaunch = (Button) findViewById(R.id.high_score_launch);

        // getting the limit for the 10th entry.
        ArrayList<Person> array = operations.getDataRecords();
        if(array.size() == MAX_SIZE) {
            score = (long)(Double.parseDouble(array.get(MAX_SIZE-1).getHighScore())*1000);
        } else {
            score = 100000;
        }

        // launching the game activity and passing the score as limiter.
        gameActivityLaunch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);

                intent.putExtra(BaseActivity.SCORE, score);
                startActivity(intent);
            }
        });

        // launching the high score activity
        highScoreActivityLaunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HighScoresListActivity.class);
                startActivity(intent);
            }
        });

        final RelativeLayout root = (RelativeLayout) findViewById(R.id.root);
        root.setBackgroundColor(Color.WHITE);
    }

    /**
     * gets the layout to be set in this activity.
     * @return id of the layout to be used.
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_game_startup;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
