package com.vikram.paddleball.utils;

import android.content.Context;
import android.util.Log;

import com.vikram.paddleball.AddHighScoreActivity;
import com.vikram.paddleball.BaseActivity;
import com.vikram.paddleball.HighScoresListActivity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class to provide operations on a collection of {@link Person Person} class objects.
 * It is a singleton class and is accessed by {@link #DataOperations(BaseActivity)#getDataOperator(BaseActivity) getDataOperator} function.
 * Created by hp-pc on 11/1/2015.
 *
 * @author Vikram Gaur
 * @version 1.1
 */
public class DataOperations {
    static private DataOperations object = null;
    private ArrayList<Person> records = null;
    private BaseActivity mActivityContext;
    private String TAG = "DataOperations";

    /**
     * Constructor for the DataOperations. Due to singleton pattern, this has been created as private member function.
     *
     * @param context reference pointing to {@link AddHighScoreActivity AddHighScoreActivity} or {@link HighScoresListActivity HighScoresListActivity}
     * @see #getDataOperator(BaseActivity)
     */
    private DataOperations(BaseActivity context) {
        records = new ArrayList();
        mActivityContext = context;
        readData();
    }

    /**
     * Used for getting the instance of this class. This function will provide only single instance whenever it is accessed by other classes.
     *
     * @param context reference pointing to {@link AddHighScoreActivity AddHighScoreActivity} or {@link HighScoresListActivity HighScoresListActivity}
     * @see #DataOperations(BaseActivity)
     */
    public static DataOperations getDataOperator(BaseActivity context) {
        if (object == null) {
            object = new DataOperations(context);
        }
        return object;
    }

    /**
     * Getter function for getting the list of Person objects. This needs to be called every time we need to perform operations on any person in the list.
     *
     * @see #setDataRecords(ArrayList)
     */
    public ArrayList<Person> getDataRecords() {
        return records;
    }

    /**
     * Setter function for setting the list of Person objects. This needs to be called every time we need to commit operations on the list of Person received from {@link #getDataRecords() getDataRecords}.
     *
     * @see #getDataRecords()
     */
    public void setDataRecords(ArrayList<Person> given) {
        records = given;
    }

    /**
     * Reading data serially from the storage file and instantiating the records. These records can now be accessed from {@link #getDataRecords() getDataRecords}.
     *
     * @see #getDataRecords()
     */
    private void readData() {
        FileInputStream inputStream;
        try {
            inputStream = mActivityContext.openFileInput("storage.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                records.add(new Person(line));
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Log.v(TAG, "File does not exist. Will create on exit.");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Overloaded function to store all the data into the file. In case new records have to be inserted, {@link #saveData(ArrayList) saveData(ArrayList)} needs to be called.
     *
     * @see #saveData(ArrayList)
     */
    public void saveData() {
        saveData(records);
    }

    /**
     * Function to store records provided into the file. In case current data has to be inserted, {@link #saveData() saveData()} needs to be called.
     *
     * @see #saveData()
     */
    public void saveData(ArrayList<Person> records) {
        FileOutputStream outputStream = null;
        try {
            outputStream = mActivityContext.openFileOutput("storage.txt", Context.MODE_PRIVATE);
            Collections.sort(records);
            int limit = records.size();
            if(limit > 10)
                limit = 10;
            // limiting to 10 members or size of the array, whichever is lesser.
            for (int i=0; i<limit; i++) {
                Person record = records.get(i);
                String s = record.toString();
                byte[] data = s.getBytes();
                outputStream.write(data, 0, data.length);
            }
            outputStream.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Not able to write to the file.");
        }
    }
}