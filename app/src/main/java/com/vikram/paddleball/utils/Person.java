package com.vikram.paddleball.utils;

import java.util.List;

/**
 * Person class to store data of different individuals.
 * Can be sorted using the Collections class method {@link java.util.Collections#sort(List) sort}
 * Created by hp-pc on 11/1/2015.
 *
 * @author Vikram Gaur
 * @version 1.1
 */
public class Person implements Comparable<Person> {
    private String name;
    private String highScore;

    /**
     * Basic constructor to be used when creating a new Person object with unknown variables.
     * The member fields can be set later using {@link #setName(String) setName}, {@link #setHighScore(String) setHighScore}.
     *
     * @see #setName(String)
     * @see #setHighScore(String)
     * @see #Person(String)
     */
    public Person() {
        name = "";
        highScore = "";
    }

    /**
     * Constructor to be used to store information while reading data from file using {@link DataOperations#readData()}.
     * The member fields can also be changed later using {@link #setName(String) setName}, {@link #setHighScore(String) setHighScore}.
     *
     * @see #setName(String)
     * @see #setHighScore(String)
     * @see #Person()
     */
    public Person(String s) {
        String[] data = s.split("\t");
        name = data[0];
        highScore = data[1];
    }

    /**
     * Getter function for getting the high score field.
     *
     * @see #setHighScore(String)
     */
    public String getHighScore() {
        return highScore;
    }

    /**
     * Setter function for setting the high score field.
     *
     * @param highScore data to be stored in the high score field.
     * @see #getHighScore()
     */
    public void setHighScore(String highScore) {
        this.highScore = highScore;
    }

    /**
     * Getter function for getting the name field.
     *
     * @see #setName(String)
     */
    public String getName() {
        return name;
    }

    /**
     * Setter function for setting the name field.
     *
     * @param name data to be stored in the name field.
     * @see #getName()
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Providing a modified string representation to the calling function.
     */
    @Override
    public String toString() {
        return name + "\t" + highScore + "\n";
    }

    /**
     * Compare function used to implement the {@link Comparable Comparable} interface. This is called when sorting an array of Person objects.
     *
     * @param another Object to be compared to.
     * @see Comparable
     */
    @Override
    public int compareTo(Person another) {
        if (another == null) {
            return -1;
        }
        Double highScore = Double.parseDouble(this.highScore);
        return highScore.compareTo(Double.parseDouble(another.getHighScore()));
    }
}