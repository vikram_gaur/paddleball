package com.vikram.paddleball;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vikram.paddleball.views.Ball;
import com.vikram.paddleball.views.Brick;
import com.vikram.paddleball.views.Paddle;

import java.util.ArrayList;

/**
 * Game Activity class having the main game.
 * Implements sensor event listener to get the sensor events from registered sensor.
 * Created by hp-pc on 11/1/2015.
 *
 * @author Vikram Gaur
 */
public class GameActivity extends Activity implements SensorEventListener {
    private long mTime;
    private long mBaseTime;
    private long mLastUpdate;
    private boolean mIsRunning = false;
    private boolean isFinishing = false;
    private int mScreenWidth, mScreenHeight;

    private Ball mBall;
    private Paddle mPaddle;
    private Button mStartButton;
    private TextView mScoreTime;
    private FrameLayout mPaddleArea;
    private FrameLayout mFrameLayout;
    private ArrayList<Brick> mBrickArray;

    private Sensor mAccelerometer;
    private SensorManager mSensorManager;

    private long comparisonScore;

    /**
     * Entry point for Game activity.
     * @param savedInstanceState If the activity is being re-initialized after
     *     previously being shut down then this Bundle contains the data it most
     *     recently supplied in {@link #onSaveInstanceState}.  Otherwise it is null.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        hideStatusBar();
        setContentView(R.layout.activity_game);

        comparisonScore = getIntent().getLongExtra(BaseActivity.SCORE, 100000);

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        mScreenWidth = size.x;
        mScreenHeight = size.y;

        RelativeLayout rootLayout = (RelativeLayout) findViewById(R.id.root_game);
        rootLayout.setBackgroundColor(Color.CYAN);
//        mRootLayout.setBackgroundResource(R.drawable.background);

        addContentView(getLayoutInflater().inflate(R.layout.game_screen, null), new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mPaddleArea = (FrameLayout) findViewById(R.id.paddle_frame);

        mPaddle = new Paddle(this, mScreenWidth);
        mPaddle.setPosition(0, mScreenHeight - getPaddlePosition());
        mPaddleArea.addView(mPaddle);

        FrameLayout parentFrame = (FrameLayout) mPaddleArea.getParent();
        parentFrame.removeView(mPaddleArea);
        rootLayout.addView(mPaddleArea);

        // adding pause screen view which can show the resume button and exit button.
        addContentView(getLayoutInflater().inflate(R.layout.pause_screen, null), new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        // main pause frame.
        mFrameLayout = (FrameLayout) findViewById(R.id.pause_frame);
        // blocking the touch listener.
        mFrameLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        Button resume = (Button) findViewById(R.id.resume_button);
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidePauseScreen();
            }
        });

        Button exit = (Button) findViewById(R.id.exit_button);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mFrameLayout.setVisibility(View.GONE);

        // Creating an instance of Ball view and setting position accordingly.
        mBall = new Ball(this, mScreenWidth);
        mBall.setPosition(mScreenWidth / 12, mScreenHeight - getPaddlePosition() - mScreenWidth / 24);
        mPaddleArea.addView(mBall);

        // Initializing the brick array with different brick view elements.
        mBrickArray = new ArrayList<>();
        for (int row = 0; row < 3; row++) {
            // every row starts fresh.
            int previous = 0;
            for (int brick = 0; brick < 9 - row; brick++) {
                int hitCount = previous;
                while (hitCount == previous) {
                    // setting different hit counts in each row.
                    hitCount = (int) (Math.random() * 5) + 1;
                }
                previous = hitCount;
                Brick b = new Brick(this, row, brick, hitCount);
                mBrickArray.add(b);
                // adding the view to paddle layout.
                mPaddleArea.addView(b);
            }
        }

        // initializing the control bar and bringing it to front of all the screens.
        RelativeLayout controlBar = (RelativeLayout) findViewById(R.id.control_layout);
        controlBar.bringToFront();
        rootLayout.requestLayout();
        rootLayout.invalidate();

        // Start button of control bar.
        mStartButton = (Button) findViewById(R.id.start_button);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resumeGame();   // FIXME: 11/25/2015
            }
        });

        // Time view of control bar.
        mScoreTime = (TextView) findViewById(R.id.time_control);

        // seek bar to control the speed of the ball.
        SeekBar seekBar = (SeekBar) findViewById(R.id.speed_control);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setSpeed(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mBall.setSpeed(seekBar.getProgress());

        // initializing the accelerometer sensor variable. Will be registered later.
        mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    /**
     * Removing the pause screen from the view to show the game again.
     */
    private void hidePauseScreen() {
        mFrameLayout.setVisibility(View.GONE);
        hideStatusBar();
        if(mBall.gameStarted())
            resumeGame();
    }

    /**
     * Setting the ball speed using the value of seek bar.
     * @param speed the speed to be set.
     */
    private void setSpeed(int speed) {
        mBall.setSpeed(speed);
    }

    /**
     * Brick has been hit and the view needs to be updated accordingly.
     * @param vertical integer value denoting whether the hit is vertical or horizontal. 1:vertical, 2:horizontal.
     * @param remove true if brick needs to be removed from the view.
     * @param b the brick instance which is giving the information.
     */
    public void changeBrick(int vertical, boolean remove, Brick b) {
        if(vertical == 1) {
            mBall.reverseVerticalDirection();
        } else {
            mBall.reverseHorizontalDirection();
        }
        if(remove) {
            b.setVisibility(View.GONE);
            mBall.unregisterBallMovementCallback(b);
            mPaddleArea.removeView(b);
            mPaddleArea.invalidate();
            mBrickArray.remove(b);
        }
    }

    /**
     * Handling the key up event of the keys passed to the activity.
     * @param keyCode the actual keycode passed.
     * @param event the event occurred
     * @return true if the key has been handled by the application.
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(mFrameLayout.getVisibility() == View.VISIBLE) {
                hidePauseScreen();
            } else {
                pauseGame();
            }
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    /**
     * Starts the timer count for storing the score.
     */
    public void startTimer() {
        if(!mIsRunning) {
            mBaseTime = System.currentTimeMillis();
            mIsRunning = true;
        }
    }

    /**
     * Stops the timer and stores the time in a variable.
     */
    public void stopTimer() {
        if (mIsRunning) {
            mIsRunning = false;
            mTime += System.currentTimeMillis() - mBaseTime;
        }
    }

    /**
     * If game is over, we need to show the high score page. In case we win and score is good enough, we need to show the activity to enter the name of the scorer.
     * @param win false if user is defeated.
     */
    public void gameOver(boolean win) {
        isFinishing = true;
        String message = win?"You Win! Congratulations!":"You Lost! Too bad!";
        Intent intent;
        finish();
        stopTimer();
        if(win && eligibleForEntry(mTime)) {
            // TODO: display win message and move to high score list
            intent = new Intent(this, AddHighScoreActivity.class);
            intent.putExtra(AddHighScoreActivity.ITEM_ARGUMENT, mTime);
        } else {
            // TODO: 11/24/2015 Too bad, tch tch... Try again?
            // FIXME: 11/24/2015
            intent = new Intent(this, HighScoresListActivity.class);
        }
        startActivity(intent);
        showToast(message);
    }

    /**
     * User's score is compared with the 10th entry in the high score list. If greater, returns true.
     * @param score the current score
     * @return true if current score is greater than the 10th entry
     */
    private boolean eligibleForEntry(long score) {
        return score < comparisonScore;
    }

    /**
     * Function to show mini messages at the bottom of the activity.
     * @param message the message to be shown.
     */
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * This function is called when the window focus changes and we need to pause the game.
     * @param hasFocus true if window focus is gained, false otherwise.
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        // TODO: pause game when lose focus and resume as soon as focus returns.
        if(!hasFocus) {
            hideStatusBar();
            pauseGame();
        }
    }

    /**
     * Function to hide the status bar.
     * Can be implemented in a later version to provide a more immersive experience.
     */
    public void hideStatusBar() {
        //TODO: can try the game without the statusbar
//        if(Build.VERSION.SDK_INT < 16) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        } else {
//            View decorView = getWindow().getDecorView();
//            int uiOptions = decorView.getSystemUiVisibility() | View.SYSTEM_UI_FLAG_FULLSCREEN;
//            decorView.setSystemUiVisibility(uiOptions);
//        }
    }

    /**
     * function to update the timer text in control bar.
     * This function will be called from the ball view control.
     */
    public void updateTimeBar() {
        if (mIsRunning) {
            mScoreTime.setText(String.format("%.2f", (System.currentTimeMillis() + mTime - mBaseTime)/1000.0));
        } else {
            mScoreTime.setText(String.format("%.2f", mTime /1000.0));
        }
    }

    /**
     * Function to resume the game after calling the {@link #pauseGame() pauseGame} function.
     */
    public void resumeGame() {
        //TODO: resume the game functionality: // FIXME: 11/23/2015
        startTimer();
//        mStartButton.setEnabled(false);
        mStartButton.setText(R.string.pause);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseGame();
            }
        });
        mBall.startGame();
    }

    /**
     * Function needed to pause the game temporarily either on losing the focus or on interruption from another application like call.
     * Game will only be resumed from resume game function.
     */
    public void pauseGame() {
        //TODO: pause the game functionality: // FIXME: 11/23/2015
        mBall.pauseGame();
        if(!isFinishing) {
            mStartButton.setText(R.string.resume);  // functionality implementation is not required as it will be taken care by pause_screen.
            stopTimer();
            mFrameLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Getter function for screen width.
     * @return screen width of the device.
     */
    public int getScreenWidth() {
        return mScreenWidth;
    }

    /**
     * Getter function for screen height.
     * @return screen height of the device.
     */
    public int getScreenHeight() {
        return mScreenHeight;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Register function for the paddle callback. This is required to get the paddle position updates.
     * @param ball {@link Ball Ball} object which needs the callbacks from paddle.
     */
    public void registerPaddleCallback(Ball ball) {
        mPaddle.registerPaddleMovementCallback(ball);
    }

    /**
     * Register function for the ball callback. Every brick object will be registered with this function so as to get updates on ball position.
     * @param brick {@link Brick Brick} object which needs the callbacks from ball.
     */
    public void registerBallCallback(Brick brick) {
        mBall.registerBallMovementCallback(brick);
    }

    /**
     * Function to get the default paddle height.
     * @return default paddle height.
     */
    public int getPaddlePosition() {
        return (int) getResources().getDimension(R.dimen.paddle_padding);
    }

    /**
     * On resume function of the activity. Here we will only register the event listener for the accelerometer.
     */
    @Override
    protected void onResume() {
        super.onResume();
        // register accelerometer
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * OnPause function of the activity. Here we unregister the event listener.
     */
    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(this);
        // unregister accelerometer
        super.onPause();
    }

    /**
     * Overridden method to obtain the events from the accelerometer sensor.
     * @param event the event received
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        if(sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0];

            long currentTime = System.currentTimeMillis();

            // filter to get changes slower.
            if(currentTime - mLastUpdate > 50) {
                mLastUpdate = currentTime;

                // modifying the horizontal movement on left and right motion of the device.
                if(x<-2) {
                    mBall.modifyHorizontalSpeed(true);
                } else if(x>2) {
                    mBall.modifyHorizontalSpeed(false);
                }
            }
        }
    }

    // unimplemented overridden method.
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
