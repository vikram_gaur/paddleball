package com.vikram.paddleball.views;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

import com.vikram.paddleball.GameActivity;

/**
 * Paddle view object.
 * Created by hp-pc on 11/21/2015.
 */
public class Paddle extends Element implements View.OnTouchListener {
    int xDelta, roundedRadius, width, height, padding;

    GameActivity mActivityContext;
    PaddleMovementCallback mPaddleMovementCallback;

    Paint p = new Paint();
    Paint brownPaint = new Paint();

    public Paddle(GameActivity context, int screenWidth) {
        super(context);

        mActivityContext = context;

        padding = 5;
        roundedRadius = 20;
        width = screenWidth / 6;
        height = screenWidth / 30;

        p.setColor(Color.YELLOW);
        brownPaint.setColor(0xff964b00);

        setOnTouchListener(this);
    }

    /**
     * function to register the paddle movement callback. Used by the ball to get the position update.
     * @param cb callback to be registered
     */
    public void registerPaddleMovementCallback(PaddleMovementCallback cb) {
        mPaddleMovementCallback = cb;
    }

    /**
     * function to unregister the paddle movement callback.
     * @param cb callback to be unregistered
     */
    public void unregisterPaddleMovementCallback(PaddleMovementCallback cb) {
        mPaddleMovementCallback = null;
    }

    /**
     * Function used to receive touch events.
     */
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int X = (int) event.getRawX();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                xDelta = X - posX;
                break;
            case MotionEvent.ACTION_UP:
                // starts the game on first action up.
                mActivityContext.resumeGame();
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                // moves the paddle according to the user touch movement
                if (X - xDelta >= 0 && X - xDelta < mActivityContext.getScreenWidth() - width) {
                    posX = X - xDelta;
                }
                if(mPaddleMovementCallback != null)
                    mPaddleMovementCallback.onPaddlePositionChanged(posX);
                break;
            default:
                break;
        }
        invalidate();
        return true;
    }

    /**
     * Draws the paddle on the canvas.
     * @param canvas used to draw the paddle
     * @throws RuntimeException
     */
    @TargetApi(21)
    @Override
    public void onDraw(Canvas canvas) throws RuntimeException {
        super.onDraw(canvas);
        canvas.drawRoundRect(posX, posY, posX + width, posY + height, roundedRadius, roundedRadius, brownPaint);
        canvas.drawRoundRect(posX + padding, posY + padding, posX + width - padding, posY + height - padding, roundedRadius, roundedRadius, p);
    }

    /**
     * Interface declaration of the paddle movement callback.
     */
    public interface PaddleMovementCallback {
        void onPaddlePositionChanged(int xPosition);
    }
}
