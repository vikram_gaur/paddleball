package com.vikram.paddleball.views;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.vikram.paddleball.GameActivity;

/**
 * Brick view object.
 * Created by hp-pc on 11/21/2015.
 *
 * @author Vikram Gaur
 */
public class Brick extends Element implements Ball.BallMovementCallback {
    int hitCount, width, height, roundedRadius = 10, padding = 5;
    int mRow, mColumn;

    GameActivity mActivityContext;

    Paint p = new Paint();
    Paint blackPaint = new Paint();

    /**
     * Constructor of the brick view.
     * @param context application context where brick is created.
     * @param row the row of the brick
     * @param column the instance of the brick in the given row
     * @param hitCount how many hits it can sustain. Color is decided accordingly.
     */
    public Brick(GameActivity context, int row, int column, int hitCount) {
        super(context);

        posX = 0;
        posY = 0;
        mRow = row;
        mColumn = column;

        blackPaint.setColor(Color.BLACK);

        int brickWidth = context.getScreenWidth() / (9 - row);
        int brickHeight = context.getScreenWidth() / 12;
        setBrickHeight(brickHeight);
        setBrickWidth(brickWidth);
        setThickness(hitCount);
        setPosition(column * brickWidth, row * brickHeight);

        context.registerBallCallback(this);
        mActivityContext = context;
    }

    /**
     * Sets the hit count.
     * @param thickness hit count
     */
    public void setThickness(int thickness) {
        hitCount = thickness;
        int color;
        switch (hitCount) {
            case 5:
                color = Color.BLACK;
                break;
            case 4:
                color = Color.RED;
                break;
            case 3:
                color = Color.GREEN;
                break;
            case 2:
                color = Color.BLUE;
                break;
            case 1:
            default:
                color = Color.WHITE;
                break;
        }
        p.setColor(color);
        invalidate();
    }

    public void setBrickWidth(int w) {
        width = w;
    }

    public int getBrickWidth() {
        return width;
    }

    public void setBrickHeight(int h) {
        height = h;
    }

    public int getBrickHeight() {
        return height;
    }

    /**
     * Draws the brick on the canvas. For border visual, 2 rectangles are drawn, one with black paint and one with the correct color.
     * @param canvas drawable canvas
     * @throws RuntimeException
     */
    @TargetApi(21)
    @Override
    public void onDraw(Canvas canvas) throws RuntimeException {
        super.onDraw(canvas);
        if (width == 0 || height == 0) {
            throw new RuntimeException("Program should call setBrickWidth() and setBrickHeight() before calling onDraw() or invalidate().");
        }

        canvas.drawRoundRect(posX, posY, posX + width, posY + height, roundedRadius, roundedRadius, blackPaint);
        canvas.drawRoundRect(posX + padding, posY + padding, posX + width - padding, posY + height - padding, roundedRadius, roundedRadius, p);
    }

    /**
     * Will hit the brick and then send the status whether brick is destroyed completely.
     *
     * @return true if brick is destroyed.
     */
    public boolean hit() {
        setThickness(hitCount - 1);
        return hitCount == 0;
    }

    /**
     * updating the ball position according to the callback.
     * @param xPosition x position of the ball
     * @param yPosition y position of the ball
     * @return true if brick is destroyed.
     */
    @Override
    public boolean onBallPositionChanged(int xPosition, int yPosition) {
        int ballRadius = mActivityContext.getScreenWidth()/24;
        boolean returnValue = false;
        int x = collision(xPosition, yPosition);
        if(x>0) {
            returnValue = hit();
            mActivityContext.changeBrick(x, returnValue, this);
        }
        return returnValue;
    }

    /**
     * Detects collision of brick and ball.
     * @param xPosition x position of the collision point
     * @param yPosition y position of the collision point
     * @return 1 if vertical collision, 2 if horizontal collision, 0 if no collision
     */
    public int collision(int xPosition, int yPosition) { // FIXME: 11/24/2015 Collision detection not proper
        if(xPosition >= posX && xPosition <= posX+getBrickWidth()) {
            if (yPosition >= posY && yPosition <= posY + getBrickHeight()) {
                int distanceFromX = Math.min(Math.abs(xPosition-posX), Math.abs(xPosition-posX-getBrickWidth()));
                int distanceFromY = Math.min(Math.abs(yPosition-posY), Math.abs(yPosition-posY-getBrickHeight()));
                return (distanceFromX>distanceFromY)?1:2;
            }
        }
        return 0;
    }
}
