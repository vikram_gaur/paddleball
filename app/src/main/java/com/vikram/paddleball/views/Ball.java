package com.vikram.paddleball.views;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.vikram.paddleball.GameActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Ball view object to be created in the game.
 * Also implements the paddle movement callback to get the position update of paddle
 * Created by hp-pc on 11/21/2015.
 *
 * @author Vikram Gaur
 */
public class Ball extends Element implements Paddle.PaddleMovementCallback {

    private static String TAG = "Ball";

    public static final int MESSAGE_GAME_START = 1;
    public static final int MESSAGE_SHOW_TIME = 2;

    int mSpeed;
    // x and y speed of the ball.
    double xDelta;
    double yDelta;
    // whether ball has moved from the paddle.
    boolean onPaddle;
    double ballRadius;

    // paddle position updated from the paddle callback.
    int collisionBlockX;
    int collisionBlockLength;

    GameActivity mActivityContext;
    ArrayList<BallMovementCallback> mBallMovementCallbackList;

    private Paint p;
    private MessageHandler mHandler;

    // Constructor for the Ball view. Specifies the radius and speed of the ball as well as the initial paddle position.
    public Ball(GameActivity context, int screenWidth) {
        super(context);

        onPaddle = true;
        yDelta = -20;
        xDelta = 10;
        collisionBlockX = 0;
        collisionBlockLength = screenWidth / 6;
        ballRadius = screenWidth/24.0;
        mSpeed = 30;

        // registering the paddle callback.
        mActivityContext = context;
        mActivityContext.registerPaddleCallback(this);

        // initializing the message handler to get the update messages for redraw and timer update.
        mHandler = new MessageHandler(this);

        p = new Paint();
        p.setColor(Color.WHITE);

        // initializing the array of ball movement callbacks to be used by the bricks for registration.
        mBallMovementCallbackList = new ArrayList<BallMovementCallback>();
    }

    /**
     * Sets the speed of the ball depending on the seek bar value passed by {@link GameActivity GameActivity}.
     * @param speed the speed passed by the application
     */
    public void setSpeed(int speed) {
        mSpeed = 35-speed;
    }

    /**
     * Code to actually start the game.
     * Passes the message to handler for starting the game.
     */
    public void startGame() {
        onPaddle = false;
        mHandler.sendEmptyMessage(MESSAGE_SHOW_TIME);
        mHandler.sendEmptyMessageDelayed(MESSAGE_GAME_START, mSpeed);
    }

    /**
     * checks if the game has started.
     * @return true if ball has left the paddle.
     */
    public boolean gameStarted() {
        return !onPaddle;
    }

    /**
     * Code to update the ball position as well as decide if the game has finished.
     * It also handles the bounce if the ball strikes the border of the game.
     */
    public void runGame() {
        mHandler.removeCallbacksAndMessages(null);

        posX += xDelta;
        posY += yDelta;

        // reverses the ball direction vertically on collision with paddle.
        if(posY == mActivityContext.getScreenHeight() - mActivityContext.getPaddlePosition() - ballRadius) {
            if(posX+ballRadius >= collisionBlockX && posX-ballRadius <= collisionBlockX+collisionBlockLength) {
                reverseVerticalDirection();
            }
        }

        // lose if paddle misses the ball.
        if(posY >= mActivityContext.getScreenHeight() - mActivityContext.getPaddlePosition() + (int)ballRadius) {
            mActivityContext.gameOver(false);
            return;
        }

        // win if ball reaches top.
        if(posY <= (int)ballRadius) {
            mActivityContext.gameOver(true);
            return;
        }

        // reverses the horizontal direction in case ball hits either side of the canvas.
        if(posX >= mActivityContext.getScreenWidth() - (int)ballRadius || posX <= (int)ballRadius) {
            reverseHorizontalDirection();
        }

        // updates the bricks using the ball movement callback.
        int x = mBallMovementCallbackList.size();
        while(x>0) {
            try {
                BallMovementCallback cb = mBallMovementCallbackList.get(x - 1);
                if (!cb.onBallPositionChanged(posX, posY)) {
                    x -= 1;
                } else {
                    Log.v(TAG, "unregistered");
                }
            } catch (IndexOutOfBoundsException ex) {
                x -= 1;
            }
        }

        mHandler.sendEmptyMessageDelayed(MESSAGE_SHOW_TIME, 5);
        mHandler.sendEmptyMessageDelayed(MESSAGE_GAME_START, mSpeed);
    }

    /**
     * pauses the game.
     */
    public void pauseGame() {
        mHandler.removeCallbacksAndMessages(null);
    }

    /**
     * reverses vertical direction of the ball along with +/- 7 degrees of randomization.
     */
    public void reverseVerticalDirection() {
        yDelta = -yDelta;
        xDelta += (int)(Math.random()*5)-2;
    }

    /**
     * reverses the horizontal direction of the ball.
     */
    public void reverseHorizontalDirection() {
        xDelta = -xDelta;
    }

    /**
     * Function to register the ball movement callback. Called by the bricks.
     * @param cb callback to be registered.
     */
    public void registerBallMovementCallback(BallMovementCallback cb) {
        mBallMovementCallbackList.add(cb);
    }

    /**
     * Function to unregister the ball movement callback
     * @param cb callback to be unregistered.
     */
    public void unregisterBallMovementCallback(BallMovementCallback cb) {
        mBallMovementCallbackList.remove(cb);
    }

    /**
     * Callback function of {@link Paddle#mPaddleMovementCallback Paddle Callback}. Updates the x position of the paddle.
     * @param xPosition x position of the paddle received from the callback.
     */
    @Override
    public void onPaddlePositionChanged(int xPosition) {
        //TODO: update ball position in case the ball is on paddle.
        if(onPaddle) {
            posX = (int)(2*ballRadius) + xPosition;
            invalidate();
        } else {
            collisionBlockX = xPosition;
        }
        Log.v(TAG, "paddlePositionChanged");
    }

    /**
     * Function used to increase the horizontal speed of the ball towards left or right.
     * @param b if true increases the speed towards right, else towards left.
     */
    public void modifyHorizontalSpeed(boolean b) {
        if(!onPaddle) {
            if (b) {
                xDelta += 2;
            } else {
                xDelta -= 2;
            }
        }
    }

    /**
     * declaring the ball movement callback interface.
     */
    public interface BallMovementCallback {
        boolean onBallPositionChanged(int xPosition, int yPosition);
    }

    /**
     * Draws the view on the canvas.
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(posX, posY, (float)ballRadius, p);
    }

    /**
     * Message handler to handle the game callbacks.
     */
    public static class MessageHandler extends Handler {

        private WeakReference<Ball> mBall;

        MessageHandler(Ball ball) {
            mBall = new WeakReference<Ball>(ball);
        }

        @Override
        public void handleMessage(Message msg) {
            Ball ball = mBall.get();
            switch (msg.what) {
                // game start message
                case Ball.MESSAGE_GAME_START:
                    Log.v(TAG, "game start");
                    ball.runGame();
                    ball.invalidate();
                    break;
                case Ball.MESSAGE_SHOW_TIME:
                    ball.mActivityContext.updateTimeBar();
                    break;
                default:
                    break;
            }
            return;
        }
    }
}
