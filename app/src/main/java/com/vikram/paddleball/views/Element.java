package com.vikram.paddleball.views;

import android.content.Context;
import android.graphics.Point;
import android.view.View;

/**
 * Base Class for all the views to be used in the game.
 * Created by hp-pc on 11/21/2015.
 */
public abstract class Element extends View {
    int posX;
    int posY;

    /**
     * Creating a base class for view.
     * @param context Activity instance in which this view is created.
     */
    public Element(Context context) {
        super(context);
    }

    /**
     * Sets the position of the view.
     * @param xPosition x position of the view.
     * @param yPosition y position of the view.
     */
    public void setPosition(int xPosition, int yPosition) {
        posX = xPosition;
        posY = yPosition;
    }

    /**
     * Gets the current position of the view.
     * @return Point object containing the x and y position.
     */
    public Point getPosition() {
        return new Point(posX, posY);
    }
}
