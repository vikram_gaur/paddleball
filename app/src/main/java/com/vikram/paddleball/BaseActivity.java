package com.vikram.paddleball;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.vikram.paddleball.utils.DataOperations;

/**
 * Base activity used to derive most of the remaining activities.
 * Created by hp-pc on 11/1/2015.
 *
 * @author Vikram Gaur
 */
public abstract class BaseActivity extends AppCompatActivity {
    private String TAG = "BaseActivity";
    public static String SCORE = "score";
    DataOperations operations;
    private Toolbar toolbar;

    /**
     * Entry function of the BaseActivity class. This needs to be called on all the activities using the {@link BaseActivity BaseActivity} super
     * @param savedInstanceState earlier saved instance of the application
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        operations = DataOperations.getDataOperator(this);
    }

    /**
     * Function to implement the back navigation. If argument passed is true, it will show the back button to provide a path back to the parent activity.
     * Else, it shows nothing.
     * @param bool boolean variable used to decide whether back icon needs to be shown on application action bar.
     */
    protected void provideBackNavigation(boolean bool) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setLogo(R.drawable.tennis_ball);
            getSupportActionBar().setDisplayHomeAsUpEnabled(bool);
        } else {
            Log.v(TAG, "toolbar is null");
        }
    }

    /**
     * Derived classes need to implement this to give the actual layout resource being used.
     * @return int the id of the layout being used.
     */
    public abstract int getLayoutResource();

    /**
     * Function to launch {@link HighScoresListActivity the high score list activity}.
     */
    protected void launchHighScoresListActivity() {
        Intent intent = new Intent(this, HighScoresListActivity.class);
        finish();
        startActivity(intent);
    }

}
