package com.vikram.paddleball;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.vikram.paddleball.utils.Person;

import java.util.ArrayList;

/**
 * Activity to show the list of high scores.
 * Created by hp-pc on 11/1/2015.
 *
 * @author Vikram Gaur
 */
public class HighScoresListActivity extends BaseActivity {
    private ArrayAdapter<Person> adapter;
    private String TAG = "HighScoresListActivity";

    /**
     * Entry point for this activity.
     * @param savedInstanceState earlier saved instance of the application
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int textSize = 22;
        provideBackNavigation(true);

        ListView list = (ListView) findViewById(R.id.contactList);
        // This adapter will be used to show all the information about the person and their high score.
        adapter = new ArrayAdapter<Person>(this, android.R.layout.simple_list_item_2, android.R.id.text2, operations.getDataRecords()) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setTextSize(textSize);
                text2.setTextSize(textSize);
                text1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                text2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                int color;

                // Setting the color to show the top 3 scores.
                if (position == 0) {
                    color = Color.parseColor("#eeb600");
                } else if (position == 1) {
                    color = Color.parseColor("#b0b0b0");
                } else if (position == 2) {
                    color = Color.parseColor("#CD7F32");
                } else {
                    color = Color.BLACK;
                }
                text1.setTextColor(color);
                text2.setTextColor(color);

                Person p = operations.getDataRecords().get(position);

                text1.setText(p.getName());
                text2.setText(p.getHighScore());
                return view;
            }
        };
        list.setAdapter(adapter);
        registerForContextMenu(list);
    }

    @Override
    protected void onResume() {
        Log.v(TAG, "onResume");
        adapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.high_score_list;
    }

    /**
     * Empty menu.
     * @param menu the action bar menu.
     * @return true always.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_high_scores, menu);
        return true;
    }

    /**
     * Called when a context menu for the {@code view} is about to be shown.
     * Unlike {@link #onCreateOptionsMenu(Menu)}, this will be called every
     * time the context menu is about to be shown and should be populated for
     * the view (or item inside the view for {@link AdapterView} subclasses,
     * this can be found in the {@code menuInfo})).
     * <p>
     * Use {@link #onContextItemSelected(android.view.MenuItem)} to know when an
     * item has been selected.
     * <p>
     * It is not safe to hold onto the context menu after this method returns.
     *
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_context_scores, menu);
    }

    /**
     * This hook is called whenever an item in a context menu is selected. Used to perform handling of the context commands.
     * <p>
     * Use {@link MenuItem#getMenuInfo()} to get extra information set by the
     * View that added this menu item.
     * <p>
     *
     * @param item The context menu item that was selected.
     * @return boolean Returns the same as superclass.
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.action_delete:
                Log.v(TAG, "deleting " + info.id);
                deleteCurrentRecord(info.id);
            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * Used for deleting the record under pointer.
     * It is removed from the data records and information is passed onto the adapter to change the view accordingly.
     * @param currentItem the record to be deleted.
     */
    private void deleteCurrentRecord(long currentItem) {
        //TODO: delete the current record from the complete set.
        ArrayList<Person> array = operations.getDataRecords();
        array.remove((int) currentItem);
        operations.setDataRecords(array);
        operations.saveData();
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
//        } else if (id == R.id.action_delete_multiple_scores) {
//            //TODO: write code to select multiple scores and delete them together.
////            onContactSelected(-1);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Handles the key commands.
     * @param keyCode the key command passed to the activity.
     * @param event The key event passed to the activity.
     * @return boolean if key is handled here, return true else pass to the super class.
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
